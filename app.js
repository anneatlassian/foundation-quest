const _ = require('lodash');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const jsonpath = require('jsonpath');
const Document = require('adf-builder').Document;
const prettyjson = require('prettyjson');
const request = require('request');
const app = express();
app.use(bodyParser.json());
app.use(express.static('.'));

function prettify_json(data, options = {}) {
  return '{\n' + prettyjson.render(data, options) + '\n}';
}

const {PORT = 8080, CLIENT_ID='FNwuRDsaou5Xzl410JUHd5sBKbPxJM0V', CLIENT_SECRET='veKp56_GlxOfbd9fjTZ2kMfpn6XzoLO_EXpGW44toVZfYXvlu4gDMXY4w5wJhZSn', ENV = 'production'} = process.env;

const API_BASE_URL = 'https://api.atlassian.com';

const JIRA = {instance: 'https://test-acalantog.atlassian.net', 
              //acalantog as admin
              auth: 'Basic YWRtaW46UnViY2FsQDM=',
              projectId: '10002',
              projectKey: 'DGR',
              issueTypeId: '10002',
              createIssue: '/rest/api/2/issue',
              search: '/rest/api/2/search',
              questJql: 'project = DGR AND resolution = Unresolved AND labels = quest order by lastViewed DESC',
              leaderBoardJql: '',

              foundation: {
                instance: 'https://foundation.internal.atlassian.com', 
                //foundation-jira-bot
                auth: 'Basic Zm91bmRhdGlvbi1qaXJhLWJvdDo2WDJrP3teMS0yRmlmIXhRck4pc2FlNi4=',
                //DGR
                projectId: '10100',
                projectKey: 'DGR',
                //Donation Match Request
                issueTypeId: '6',
                createIssue: '/rest/api/2/issue',
                charityName: 'summary',
                amount: 'customfield_10402',
                currency: 'customfield_10100',
                financeUrl: 'customfield_10003',
                foundationCountry: 'customfield_11203',
                office: 'customfield_11203',
                initiative: 'customfield_11301',
              }
            };

let officeMap = new Map();
officeMap.set('11407', 'Amsterdam');
officeMap.set('11408', 'Austin');
officeMap.set('11409', 'Manila');
officeMap.set('11410', 'Mountain View');
officeMap.set('11411', 'San Francisco');
officeMap.set('11412', 'Sydney');
officeMap.set('11413', 'Remote');

let componentsMap = new Map();
componentsMap.set('11407', { "id": "10203","name": "Netherlands"});
//AUSTIN
componentsMap.set('11408', {"id": "10204","name": "United States"});
componentsMap.set('11409', {"id": "10202","name": "Manila"});
//MTV
componentsMap.set('11410', {"id": "10204","name": "United States"});
//SF
componentsMap.set('11411', {"id": "10204","name": "United States"});
componentsMap.set('11412', {"id": "10200","name": "Australia"});
//Remote
componentsMap.set('11413', {"id": "10204","name": "United States"});

let currencyMap = new Map();
currencyMap.set('10100', 'AUD');
currencyMap.set('10101', 'EUR');
currencyMap.set('10702', 'GBP');
currencyMap.set('10701', 'JPY');
currencyMap.set('10200', 'PHP');
currencyMap.set('10102', 'USD');

let initiativeMap = new Map();
initiativeMap.set('11500', 'Yes');
initiativeMap.set('11501', 'No');


if (!PORT) {
  console.log("Usage:");
  console.log("PORT=<http port> node app.js");
  process.exit();
}

function getAccessToken(callback) {
  const options = {
    uri: 'https://auth.atlassian.com/oauth/token',
    method: 'POST',
    json: {
      grant_type: "client_credentials",
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      "audience": "api.atlassian.com"
    }
  };
  request(options, function (err, response, body) {
    if (response.statusCode === 200 && body.access_token) {
      callback(null, body.access_token);
    } else {
      callback("could not generate access token: " + JSON.stringify(response));
    }
  });
}

function sendMessage(cloudId, conversationId, messageTxt, callback) {
  getAccessToken(function (err, accessToken) {
    if (err) {
      callback(err);
    } else {
      const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
      const options = {
        uri: uri,
        method: 'POST',
        headers: {
          authorization: "Bearer " + accessToken,
          "cache-control": "no-cache"
        },
        json: {
          body: {
            version: 1,
            type: "doc",
            content: [
              {
                type: "paragraph",
                content: [
                  {
                    type: "text",
                    text: messageTxt
                  }
                ]
              }
            ]
          }
        }
      }

      request(options, function (err, response, body) {
        callback(err, body);
      });
    }
  });
}

function sendReply(message, replyTxt, callback) {
  const cloudId = message.cloudId;
  const conversationId = message.conversation.id;
  const userId = message.sender.id;

  sendMessage(cloudId, conversationId, replyTxt, function (err, response) {
    if (err) {
      console.log('Error sending message: ' + err);
      callback(err);
    } else {
      callback(null, response);
    }
  });
}

app.get('/healthcheck', function (req, res) {
  res.sendStatus(200);
});

app.post('/installed',
  function (req, res) {
    console.log('app installed in a conversation');
    const cloudId = req.body.cloudId;
    const conversationId = req.body.resourceId;
    sendMessage(cloudId, conversationId, "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message", function (err, response) {
      if (err)
        console.log(err);
    });
    res.sendStatus(204);
  }
);

/**
 * Simple library that wraps the Stride REST API
 */
const stride = require('./stride').factory({
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  env: ENV
});

app.post('/bot-mention',
  function (req, res) {
    console.log('bot mention');
    let data = req.body;
    let message = data.message;

    console.log(message.text.split(' '));
    sendReply(req.body, "Hey, what's up? (Sorry, that's all I can do)", function (err, response) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        res.sendStatus(204);
      }
    });
  }
);

/*
 * This adds a glance to the sidebar. When the user clicks on it, Stride opens the module whose key is specified in "target".
 *
 * When a user first opens a Stride conversation where the app is installed,
 * the Stride app makes a REST call to the queryURL to get the initial value for the glance.
 * You can then update the glance for a conversation at any time by making a REST call to Stride.
 * Stride will then make sure glances are updated for all connected Stride users. 
 */

app.get('/game/glance',
  // cross domain request
  cors(),
  stride.validateJWT,
  (req, res) => {
    res.send(
      JSON.stringify({
        "label": {
          "value": "Foundation Game"
        }
      }));
  }
);

/**
* When a user clicks on the glance, Stride opens an iframe in the sidebar, and loads a page from your app,
* from the URL specified in the app descriptor 
**/

app.get('/game/sidebar',
  stride.validateJWT,
  (req, res) => {
    console.log('loading sidebar..');
    res.redirect("/game-sidebar.html");
  }
);

async function getIssue(options) {
  console.log('Getting issue with: ' + prettify_json(options));
  let issues = await request(options, function (error, response, body) {
      if (error) throw new Error(error);
    });

  return issues;
}

app.get('/game/quest',
//  stride.validateJWT,
  (req, res) => {
    console.log('Getting quests..');
    
    let quests;

    populateQuest();

    async function populateQuest() {
      var options = { method: 'GET',
                    url: JIRA.instance + JIRA.search,
                    qs: { jql: JIRA.questJql },
                    headers: 
                    { 'cache-control': 'no-cache',
                      'content-type': 'application/json',
                      authorization: JIRA.auth },
                    body: 
                    { startAt: 0,
                      maxResults: 15,
                      fields: [ 'summary', 'status', 'assignee' ],
                      fieldsByKeys: false },
                    json: true };

      request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log('Send: ' + body);
            res.setHeader('Content-Type', 'application/json');
            res.status(200).json(body);
      });
    }
  }
);

app.get('/game/leaderboard',
  stride.validateJWT,
  (req, res) => {
    console.log('loading sidebar..');
    res.redirect("/game-sidebar.html");
  }
);

/* All donation related */
app.get('/game/dialog',
  stride.validateJWT,
  (req, res) => {
    console.log('loading dialog...');
    res.redirect("/game-dialog.html");
  }
);


function objectifyForm(formArray) {//serialize data function
  var returnArray = {};
  for (var i = 0; i < formArray.length; i++){
    returnArray[formArray[i]['name']] = formArray[i]['value'];
  }
  return returnArray;
}

app.post('/game/donate',
  stride.validateJWT,
  (req, res) => {
    console.log('Received a call from the app frontend for donation ' + prettify_json(req.body));
    
    let data = objectifyForm(req.body);
    let reqBody = req.body;

    console.log(data);
    console.log(res.locals.context);

    const cloudId = res.locals.context.cloudId;
    const conversationId = res.locals.context.conversationId;
    const userId = res.locals.context.userId;

    let user;
    let ticketUrl;

    reqBody.cloudId = res.locals.context.cloudId
    reqBody.conversation = {};
    reqBody.conversation.id = res.locals.context.conversationId
    
    stride.replyWithText({reqBody, text: "Processing donation.."})
            .then(() => res.sendStatus(200))
            .then(donate(data))
            .catch(err => console.error('  Something went wrong', prettify_json(err)));

    async function allDone() {
      await stride.replyWithText({reqBody, text: "Awesome! Generous Human. Get your donation: " + ticketUrl});
      console.log("- all done.");
    }

    async function donate(data) {
      console.log('Inside donate async');

      let callback = async function() {
        successfulDonationCard();
      }

      createFoundationIssue(data, callback);
    }
    
    async function successfulDonationCard() {
      console.log('successfulDonationCard');
      //user = await stride.getUser({cloudId, userId});
      
      const doc = new Document();

      const card = doc.applicationCard('Success!')
        .link(ticketUrl)
        .description('Thanks for your donation! Click on this card to view');
      card.detail()
        .title('Type')
        .text('Donation')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
      card.detail()
        .title('User')
        .text(user.displayName)
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });

      const document = doc.toJSON();

      console.log('Replying to Stride with card: ' + JSON.stringify(document));
      await stride.reply({reqBody, document});
    }

    async function createFoundationIssue(data, callback) {
      user = await stride.getUser({cloudId, userId});
      console.log('User: '+prettify_json(user));

      let currencyName = currencyMap.get(data.currency);
      let officeName = officeMap.get(data.office);
      let initiativeName = initiativeMap.get(data.initiative);
      let component = componentsMap.get(data.office);

      var options = { method: 'POST',
                      url: JIRA.foundation.instance+JIRA.foundation.createIssue,
                      headers: 
                      { 'cache-control': 'no-cache',
                        'content-type': 'application/json',
                        authorization: JIRA.foundation.auth },
                      body: 
                      { fields: 
                          { project: { id: JIRA.foundation.projectId },
                            summary: data.charityName + '; ' + user.userName,
                            issuetype: { id: JIRA.foundation.issueTypeId },
                            customfield_10402: parseFloat(data.amount),
                            customfield_10100: { value: currencyName, id: data.currency },
                            customfield_10003: data.financeUrl,
                            customfield_11203: { value: officeName, id: data.office },
                            customfield_11301: { value: initiativeName, id: data.initiative },
                            customfield_11504: data.ein,
                            customfield_10012: (data.dueDate),
                            components: [ { id: component.id, name: component.name } ] } },
                      json: true };

        ticketUrl = await request(options, function (error, response, body) {
          if (error) throw new Error(error);

          ticketUrl = JIRA.foundation.instance+'/browse/'+body.key;
          console.log(ticketUrl);
          callback();
          
        });

    }

    async function createJiraIsse(data) {
      user = await stride.getUser({cloudId, userId});
      console.log(prettify_json(user));
      var options = { method: 'POST',
                      url:  JIRA.instance+JIRA.createIssue,
                      headers: 
                      { 'cache-control': 'no-cache',
                        'content-type': 'application/json',
                        authorization: JIRA.auth },
                      body: 
                      { fields: 
                          { project: { id: JIRA.foundation.projectId },
                            summary: 'Amount: ' + data.amount + '; urlLink: ' + data.urlLink,
                            description: 'User who invoked: ' + user.userName,
                            issuetype: { id: JIRA.issueTypeId },
                             } 
                       },
                      json: true };

      await requestCreate(options);
    }

    function requestCreate(options) {
      return new Promise(function(resolve, reject) {
        console.log('Entering request Promise');
        request(options, function (error, response, body) {
          if (error) throw new Error(error);

          console.log(body);
          ticketUrl = JIRA.instance+'browse/'+body.self;
          console.log('Resolving ' + ticketUrl);
          resolve(ticketUrl);
        });
      });
    }
  }
);


/* All Quest related */

app.post('/ui/ping',
  stride.validateJWT,
  (req, res) => {
    console.log('Received a call from the app frontend ' + prettify_json(req.body));
    const cloudId = res.locals.context.cloudId;
    const conversationId = res.locals.context.conversationId;

    console.log('cloudId='+cloudId +'; conversationId='+conversationId);
    stride.sendTextMessage({cloudId, conversationId, text: "Pong"})
      .then(() => res.send(JSON.stringify({status: "Pong"})))
      .catch(() => res.send(JSON.stringify({status: "Failed"})))
  }
);

app.post('/game/join',
  stride.validateJWT,
  (req, res) => {
    /*
    console.log('Joining Game ' + prettify_json(req.body));
    
    const cloudId = res.locals.context.cloudId;
    const conversationId = res.locals.context.conversationId;


    stride.sendTextMessage({cloudId, conversationId, text: "Joining Quest.."})
      // If you don't send a 200 fast enough, Stride will resend you the same mention message
      .then(() => res.sendStatus(200))
      // Now let's do the time-consuming things:
      .then(() => showGameCard({reqBody}))
      .then(allDone)
      .catch(() => res.send(JSON.stringify({status: "Failed"})));

      async function allDone() {
        await stride.replyWithText({reqBody, text: "OK, I'm done. Thanks for watching!"});
        console.log("- all done.");
      }
      */

      console.log('Received a call from the app frontend ' + prettify_json(req.body));
      const cloudId = res.locals.context.cloudId;
      const conversationId = res.locals.context.conversationId;

      let card = {
        "type": "applicationCard",
        "attrs": {
          "text": "Donovan Kolbly updated a file: applicationCard.md",
          "title": {
            "text": "Donovan Kolbly updated a file: applicationCard.md"
          }
        }
      };
  
      console.log('cloudId='+cloudId +'; conversationId='+conversationId);
      stride.sendTextMessage({cloudId, conversationId, text: "Joining Quest.."})
        //.then(() => res.send(JSON.stringify({status: "Pong"})))
        //.then(() => res.sendStatus(200))
        // Now let's do the time-consuming things:
        .then(() => res.send(JSON.stringify(card)))
        .then(() => res.sendStatus(200))
        .catch(() => res.send(JSON.stringify({status: "Failed"})))

        async function allDone() {
          await stride.replyWithText({reqBody, text: "OK, I'm done. Thanks for watching!"});
          console.log("- all done.");
        }
  }
);

async function showGameCard({reqBody}) {
  console.log('loading game card..');

  //await stride.replyWithText({reqBody, text: "Sending a message with plenty of formatting..."});
  await addCard()

  async function addCard() {
    doc
    .paragraph()
    .text("And a card");
    const card = doc.applicationCard('With a title')
      .link('https://www.atlassian.com')
      .description('With some description, and a couple of attributes');
    card.detail()
      .title('Type')
      .text('Task')
      .icon({
        url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
        label: 'Task'
      });
    card.detail()
      .title('User')
      .text('Joe Blog')
      .icon({
        url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
        label: 'Task'
      });

    const document = doc.toJSON();

    await stride.reply({reqBody, document});
  }
}


//PAYPAL related routes

const SANDBOX_ACCT = 'rubyannecalantog-facilitator@gmail.com';
const SANDBOX_CLIENT_ID = 'ARlMH7h2PjpMvSkrqolKGTiSSRMURsG4cKS5c9LFs6RdZB2wYclte2X9v0MqsTEfsptjy7kTFmTHT-p6';
const SANDBOX_SECRET = 'EAv8PS09Ob7VrCkMQ2AzAsIEoYbmZl2Fa_SmT2imFd3zRkSh7oqE1FrghFdEu1Zuun5xndjMU8n5OieJ';
const SANDBOX_AUTH = 'Basic QVJsTUg3aDJQanBNdlNrcnFvbEtHVGlTU1JNVVJzRzRjS1M1YzlMRnM2UmRaQjJ3WWNsdGUyWDl2ME1xc1RFZnNwdGp5N2tURm1USFQtcDY6RUF2OFBTMDlPYjdWckNrTVEyQXpBc0lFb1libVpsMkZhX1NtVDJpbUZkM3pSa1NoN29xRTFGcmdoRmRFdTFadXVuNXhuZGpNVThuNU9pZUo=';

const PAYPAL_API = 'https://api.sandbox.paypal.com';
//paypal: get access token
function getPaypalAccessToken(callback) {
  const options = {
    uri: PAYPAL_API+'/v1/oauth2/token',
    method: 'POST',
    headers: {
      authorization: SANDBOX_AUTH,
      'content-type': 'application/x-www-form-urlencoded',
      "cache-control": "no-cache"
    },
    form: { grant_type: 'client_credentials' }
  };
  request(options, function (err, response, body) {
    if (response.statusCode === 200 && body.access_token) {
      callback(null, body.access_token);
    } else {
      callback("could not generate access token: " + JSON.stringify(response));
    }
  });
}

app.post('/paypal', function(req, res){
  console.log('Paypal webhook');
  console.log(req);
});

/**
 * Don't worry about this for now.
 */
app.get('/descriptor', function (req, res) {
  fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
    const template = _.template(descriptorTemplate);
    const descriptor = template({
      host: 'https://' + req.headers.host
    });
    res.set('Content-Type', 'application/json');
    res.send(descriptor);
  });
});

http.createServer(app).listen(PORT, function () {
  console.log('App running on port ' + PORT);
});
